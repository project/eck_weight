ECK Weight Module
------------------------------

Description
-----------
This module adds weight functionality for entities implemented with ECK.

Installation 
------------
 + Copy the module's directory to your modules directory.
 + Activate the module.
